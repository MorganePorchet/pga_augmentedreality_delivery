using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class touchBehavior : MonoBehaviour
{
    Animator theAnimator;
    Touch theTouch;

    // Start is called before the first frame update
    void Start()
    {
        theAnimator = this.GetComponent<Animator>();       // reference to the animator
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            theTouch = Input.GetTouch(0);       // infos of the first touch
            if (theTouch.phase == TouchPhase.Began)
            {
                theAnimator.SetBool("screenTouched", true);
            }
            if (theTouch.phase == TouchPhase.Ended)
            {
                theAnimator.SetBool("screenTouched", false);
            }
        }
    }
}
