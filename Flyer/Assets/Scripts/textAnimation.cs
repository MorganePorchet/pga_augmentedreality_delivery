using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textAnimation : MonoBehaviour
{
    Animator theAnimator;
    GameObject theText;
    Touch theTouch;
    GameObject child1;
    GameObject child2;
    GameObject child3;
    int count = 0;
    
    // Start is called before the first frame update
    void Start() {
        theText = this.GetComponent<GameObject>();
        child1 = GameObject.Find("TextInfotronics");
        child1.SetActive(false);
        theAnimator = this.GetComponent<Animator>();
        Debug.Log("child 1 state : " + child1.activeSelf);
    }

    // Update is called once per frame
    void Update() {
        
        if (Input.touchCount > 0) {
            Debug.Log("child 1 state : " + child1.activeSelf);
            theTouch = Input.GetTouch(0);
            if (theTouch.phase == TouchPhase.Began) {
                count++;
                switch (count) {
                    case 1:
                        theAnimator.SetBool("oneTouch", true);
                        break;
                    case 2:
                        theAnimator.SetBool("twoTouch", true);
                        break;
                    case 3:
                        theAnimator.SetBool("threeTouch", true);
                        break;
                    case 4:
                        count = 0;
                        theAnimator.SetBool("fourTouch", true);
                        break;
                    default:
                        break;
                }
            }
            if (theTouch.phase == TouchPhase.Ended) {

                theAnimator.SetBool("twoTouch", false);
                theAnimator.SetBool("oneTouch", false);
                theAnimator.SetBool("threeTouch", false);
                theAnimator.SetBool("fourTouch", false);
            }
        }
    }
}
