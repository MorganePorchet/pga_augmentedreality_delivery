# PGa - Augmented Reality
In this git repository you will find 3 folders. Each one corresponds to one part of the PGa - Augmented Reality project : Flyer, Hololens and Automation.

In this document you will find the steps to run each of the 3 projects on Unity.

## Flyer
In this folder you will find the _Assets_, _Packages_, _Project Settings_ and _TrackedImages_ folders needed in a Unity project. You will also find the different application versions as _.apk_ files. The final version is the **v3_4**.

To run this project, please follow these steps :
1. Install Unity [**2021.3.11f1** version](https://unity.com/releases/editor/archive)
2. Install [Vuforia Engine](https://developer.vuforia.com/downloads/SDK) package
3. Open the cloned project
4. Print out the tracking images that can be found in the folder _TrackedImages_
5. Connect the Android device (working on **Android 8.0 'Oreo'** or higher)
    - Developer mode must be on
    - USB file transfer must be enabled
6. Build and run the project
7. See the result on the Android device

## Automation
In this folder you will find the following folders:
- _Augmented_Reality_Python_ : containing the scripts for the ball-tracking, the communication with the robot and the communication with the Unity application
- _Augmented_Reality_Robot_ : containing the program for the robot and the configurations needed to use the labyrinth
- _Augmented_Reality_Unity_ : containing the actual Unity project
- _Mechanic_ : containing the 3D files of the labyrinth
- _URSim_ : containing the VM for the robot programing software

To run the project please follow the instructions of the _Tutoriel Utilisation PGa Augmented Reality_ file.
This project works on Unity [**2021.2.10f1** version](https://unity.com/releases/editor/archive).
The QR Code to print out is in the _QR_ file.

## Hololens
In this folder you will find the  _Assets_, _Meshes_, _Packages_ and _Project Settings_ folders needed in a Unity project.
To run this project, follow this [tutorial](https://learn.microsoft.com/en-us/training/modules/learn-mrtk-tutorials/1-1-introduction) beginning at the end of step 8.
This project works on Unity [**2021.3.14f**](https://unity.com/releases/editor/archive).
