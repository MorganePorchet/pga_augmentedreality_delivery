#!/usr/bin/env python
# Copyright (c) 2016, Universal Robots A/S,
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the Universal Robots A/S nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL UNIVERSAL ROBOTS A/S BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys
from datetime import time
import time

sys.path.append('..')
import logging

import rtde.rtde as rtde
import rtde.rtde_config as rtde_config

positiony = 0;
#logging.basicConfig(level=logging.INFO)

#IP simulation
#ROBOT_HOST = '192.168.239.132'

#IP real
ROBOT_HOST = '192.168.0.105'

ROBOT_PORT = 30004
config_filename = 'control_loop_configuration.xml'

keep_running = True

logging.getLogger().setLevel(logging.INFO)

conf = rtde_config.ConfigFile(config_filename)
state_names, state_types = conf.get_recipe('state')
setp_names, setp_types = conf.get_recipe('setp')
watchdog_names, watchdog_types = conf.get_recipe('watchdog')

con = rtde.RTDE(ROBOT_HOST, ROBOT_PORT)
con.connect()

# get controller version
con.get_controller_version()

# setup recipes
con.send_output_setup(state_names, state_types)
setp = con.send_input_setup(setp_names, setp_types)
watchdog = con.send_input_setup(watchdog_names, watchdog_types)

# Setpoints to move the robot to
#Numbers are respectively X, Y, Z, rX, Ry, Rz, tool, wait,vacumm)
#View points
setp1 = [-0.200, -0.108, 0.4, 0, 0, 0.340,0,0,2]
setp2 = [-0.200, -0.108, 0.399, 0, 0, 0.340,0,0,2]

#catch the vaccum
setp3 = [0.0, -0.30, 0.35, 0, 0, 0.34,1,0,1]
setp4 = [-0.027, -0.379, 0.18, 0.0, 0.0, 0.340,1,1,0]
setp5 = [-0.027, -0.379, 0.062, 0, 0, 0.340,1,1,0]
setp6 = [-0.027, -0.379, 0.18, 0.0, 0.0, 0.340,0,1,0]
#vaccum catched
setp5 = [-0.350, -0.253, 0.080, 0, 0, 0.340,0,1,0]
setp6 = [-0.350, -0.253, 0.045, 0, 0, 0.340,0,1,1]
setp7 = [-0.350, -0.253, 0.060, 0, 0, 0.340,0,1,1]


setp8 = [0.0, -0.3, 0.25, 0, 0, 0.34,0,0,1]
setp9= [0.35, -0.253, 0.18, 0, 0, 0.340,0,1,1]
setp10= [0.35, -0.253, 0.048, 0, 0, 0.340,0,1,1]
setp11= [0.35, -0.253, 0.18, 0, 0, 0.340,0,1,2]
#release the VACCUM
setp12 = [0.0, -0.35, 0.30, 0, 0, 0,0,0,0]
setp13 = [-0.027, -0.379, 0.12, 0.0, 0.0, 0.340,0,1,1]
setp14 = [-0.027, -0.379, 0.062, 0, 0, 0.340,0,1,1]
setp15 = [-0.027, -0.379, 0.12, 0.0, 0.0, 0.340,1,1,1]
setp16 = [0.0, -0.3, 0.12, 0, 0, 0,1,0,0]

countersetp = 0;

setp.input_double_register_0 = 0
setp.input_double_register_1 = 0
setp.input_double_register_2 = 0
setp.input_double_register_3 = 0
setp.input_double_register_4 = 0
setp.input_double_register_5 = 0
setp.input_double_register_6 = 0
setp.input_double_register_7 = 0
setp.input_double_register_8 = 0





# The function "rtde_set_watchdog" in the "rtde_control_loop.urp" creates a 1 Hz watchdog
watchdog.input_int_register_0 = 1

def setp_to_list(setp):
    list = []
    for i in range(0,9):
        list.append(setp.__dict__["input_double_register_%i" % i])
    return list

def list_to_setp(setp, list):
    for i in range (0,9):
        setp.__dict__["input_double_register_%i" % i] = list[i]
    return setp

#start data synchronization
if not con.send_start():
    sys.exit()

# control loop
while keep_running:
    # receive the current state
    state = con.receive()
   
    
    if state is None:
        break;
    
    # do something...

    if state.output_int_register_0 != 0:
        """
        if setp_to_list(setp) == setp1:
            new_setp = setp2

        elif setp_to_list(setp) == setp2:
            new_setp = setp3
        elif setp_to_list(setp) == setp3:
            new_setp = setp4
        elif setp_to_list(setp) == setp4:
            new_setp = setp5
        elif setp_to_list(setp) == setp5:
            new_setp = setp6
        elif setp_to_list(setp) == setp6:
            new_setp = setp7
        elif setp_to_list(setp) == setp7:  # useless but don't know why
            new_setp = setp1
        else:
            new_setp = setp1
        """
         #djd

        # fourchettes couteau
        if setp_to_list(setp) == setp1:
            new_setp = setp2

        elif setp_to_list(setp) == setp2:
            new_setp = setp3
        elif setp_to_list(setp) == setp3:
            new_setp = setp4
        elif setp_to_list(setp) == setp4:
            new_setp = setp5
        elif setp_to_list(setp) == setp5:
            new_setp = setp6
        elif setp_to_list(setp) == setp6:
            new_setp = setp7
        elif setp_to_list(setp) == setp7:
            new_setp = setp8
        elif setp_to_list(setp) == setp8:
            new_setp = setp9
        elif setp_to_list(setp) == setp9:
            new_setp = setp10
        elif setp_to_list(setp) == setp10:
            new_setp = setp11
        elif setp_to_list(setp) == setp11:
            new_setp = setp12
        elif setp_to_list(setp) == setp12:
            new_setp = setp13
        elif setp_to_list(setp) == setp13:
            new_setp = setp14
        elif setp_to_list(setp) == setp14:
            new_setp = setp15
        elif setp_to_list(setp) == setp15:
            new_setp = setp16
        elif setp_to_list(setp) == setp16:
            new_setp = setp1
        else :
            new_setp = setp1


        """  
                countersetp +=1
                   new_setp = setp+countersetp
                   if countersetp > 5:
                       countersetp = 1
                       """

        #new_setp = setp1 if setp_to_list(setp) == setp2 else setp2

        list_to_setp(setp, new_setp)
        print(new_setp)

        # send new setpoint
    con.send(setp)
    # kick watchdog
    con.send(watchdog)

con.send_pause()

con.disconnect()
