'''
#!/usr/bin/env python
# Copyright (c) 2016, Universal Robots A/S,
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the Universal Robots A/S nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL UNIVERSAL ROBOTS A/S BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
#######################################################################################################################
#                       Augmented Reality, Project and Applied Management 2022-2023                                   #
#                      Robot process and communications coded by Florent Stragiotti                                   #
#                                Image tracking coded by Maxime Caesali                                               #
#######################################################################################################################

import sys
from threading import Thread
from pathlib import Path
import logging
import rtde.rtde as rtde
import rtde.rtde_config as rtde_config
from Ball_Tracking import ball_tracking
import socket
import time

needPos = True
needPosString = str(needPos)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
input_str = ""


def setp_to_list(setp):
    temp = []
    for i in range(0, 6):
        temp.append(setp.__dict__["input_double_register_%i" % i])
    return temp


def list_to_setp(setp, list):
    for i in range(0, 6):
        setp.__dict__["input_double_register_%i" % i] = list[i]
    return setp


def getUnityValue(sock):
    # To avoid the possibility that the thread can execute read / send message on the socket at the same time
    sock.sendall(needPosString.encode("UTF-8"))  # Converting string to Byte, and sending it to C# (Unity)
    receivedData = sock.recv(1024).decode("UTF-8")  # receiving data in Byte from C#, and converting it to String

    # Formatting the response
    temp = receivedData.split(';')
    pos = [float(temp[0]), float(temp[1]), float(temp[2])]
    # print('Rotations read by the Unity script : Rx =', pos[0], '; Ry =', pos[1], '; Rz =', pos[2])

    for i in range(3):
        pos[i] = round(pos[i], 1)
        if pos[i] >= 180:
            pos[i] = (pos[i] - 360)
    # print('Rotations converted to negative degree : Rx =', pos[0], '; Ry =', pos[1], '; Rz =', pos[2])

    # Max rotation Rx to avoid robot to collide to the table or be in singularity position : +/-0.4 rad -> +-23°
    max_rotation = 20  # To be safe we go 3 degree less than the limit
    if pos[0] > max_rotation:
        pos[0] = max_rotation
    if pos[0] < -max_rotation:
        pos[0] = -max_rotation

    Rx = format(pos[0], '.1f')
    Ry = format(pos[1], '.1f')
    Rz = 0.0
    # print('Rotations transmit in degrees : Rx =', Rx, '; Ry =', Ry, '; Rz =', Rz)

    pos = [pos[0] * 2 * 3.1415 / 360, pos[1] * 2 * 3.1415 / 360, pos[2] * 2 * 3.1415 / 360]
    # print('Rotations transmit in radians : Rx =', pos[0], '; Ry =', pos[1], '; Rz =', 0)
    return pos


def wait_key():
    global input_str
    while True:
        input_str = input()


def runRobot(sock):
    # Starting communication between Python and Robot
    print("Starting robot communication")
    #ROBOT_HOST = '192.168.133.129'  # Simulation
    ROBOT_HOST = '10.30.5.153'      # Real robot
    ROBOT_PORT = 30004

    config_filename = 'control_loop_configuration.xml'

    logging.getLogger().setLevel(logging.INFO)
    conf = rtde_config.ConfigFile(str((Path(__file__).parent / Path(config_filename)).resolve().absolute()))
    state_names, state_types = conf.get_recipe('state')
    setp_names, setp_types = conf.get_recipe('setp')
    watchdog_names, watchdog_types = conf.get_recipe('watchdog')
    mode_names, mode_types = conf.get_recipe('mode')

    con = rtde.RTDE(ROBOT_HOST, ROBOT_PORT)
    con.connect()
    print('Robot communication established')

    # setup recipes
    con.send_output_setup(state_names, state_types)
    setp = con.send_input_setup(setp_names, setp_types)
    watchdog = con.send_input_setup(watchdog_names, watchdog_types)
    mode = con.send_input_setup(mode_names, mode_types)

    # Start data synchronization
    if not con.send_start():
        sys.exit()

    # Home position
    # Numbers are respectively X, Y, Z, Rx, Ry, Rz)
    # Tool params are : Z = 375mm Rx = -1.571rad payload = 3kg Cz = 150mm      # A MODIFIER POUR QUE CA SOIT CORRECT !!!
    # Best parameters to maximise Rx movement
    X = 0.114
    Y = -0.6100
    Z = 0.2700
    RX = 0
    RY = 0
    RZ = 0

    STARTING_POINT = [X, Y, Z, RX, RY, RZ]

    # Initialise all setp.input_double_register_0 to 5 at the value of 0
    for i in range(0, 6):
        setp.__dict__["input_double_register_%i" % i] = 0

    # The function "rtde_set_watchdog" in the "Augmented_Reality_project" creates a 1 Hz watchdog
    watchdog.input_int_register_0 = 0

    # Define the mode constant to be more readable
    BOOTING = 0
    INIT = 1
    TRACKING = 2
    STOP = 3

    # First state is BOOTING state. So we transmit it to the robot. (He should already be in this mode when starting)
    mode.input_int_register_1 = BOOTING
    current_mode = BOOTING
    con.send(mode)

    # When AR tracking, value is actualised each 20ms (50Hz)
    REQUEST_TIME = 1 / 50  # 50 Hz

    keep_running = True
    printOnce = True

    # Control loop
    while keep_running:

        # Receive the current state
        state = con.receive()
        # Set the mode according to the robot process
        mode.input_int_register_1 = state.output_int_register_0
        if current_mode != mode.input_int_register_1:  # We allow to print text only when mode is changing
            current_mode = mode.input_int_register_1
            printOnce = True
        con.send(mode)

        if current_mode == BOOTING:
            if printOnce:
                print('Booting mode, click Continue on the robot screen popup to start the movement')
                printOnce = False

        elif current_mode == INIT:  # The robot go to his home position
            list_to_setp(setp, STARTING_POINT)
            con.send(setp)
            if printOnce:
                print('Init mode, going to home position :', STARTING_POINT)
                printOnce = False

            # Preparing values for the next mode "TRACKING" when switching to limit dead time
            t_start = time.time()
            pos = getUnityValue(sock)

        elif current_mode == TRACKING:  # Follows the rotations of the AR
            if printOnce:
                print('Tracking mode, you can move the plate in any orientation you want')
                print('If you want to quit this mode and stop the program, type "Stop" with uppercase')
                printOnce = False
                # We start the thread here to avoid a problem if another 'input()' is called at the same time
                input_thread.start()

            if time.time() - t_start > REQUEST_TIME:
                pose = [X, Y, Z, pos[0] + RX, pos[1] + RY, 0 + RZ]
                list_to_setp(setp, pose)
                con.send(setp)
                t_start = time.time()
                pos = getUnityValue(sock)  # Preparing next value to transmit to limit the dead time of receiving data
            if input_str == 'Stop':
                current_mode = STOP
                mode.input_int_register_1 = current_mode
                con.send(mode)
                print('Stopping the process')
                break
        else:
            print('Error in process, mode number unknown')
            break  # If another value than 0, 1, 2 or 3 is in the mode, there is a problem, so we stop the robot.

        con.send(watchdog)
    con.send_pause()
    con.disconnect()
    sock.close()
    print('End of process, thank you !')


if __name__ == "__main__":
    while input('Please (re)start the Unity simulation and the robot program, and then write "Done"\n') != "Done":
        print('Write "Done" with uppercase please')
    # Starting communication between Unity and Python (Have to start here 'cause this connection is used by both threads
    print('Starting Unity communication')
    host, port = "127.0.0.1", 25001
    sock.connect((host, port))
    print('Unity communication established')

    # create the thread
    robot_thread = Thread(target=runRobot, args=(sock,))
    tracking_thread = Thread(target=ball_tracking, args=(sock,))
    input_thread = Thread(target=wait_key)

    # start the thread
    robot_thread.start()
    tracking_thread.start()

    # wait for the thread to complete
    robot_thread.join()
    tracking_thread.join()
    input_thread.join()
