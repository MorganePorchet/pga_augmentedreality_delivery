import cv2
import numpy as np


def ball_tracking(sock):
    # opening the camera (device 0 = PC webcam , devive 1 = IR camea , device 2 = External wbcam) (PC Maxime)
    # device 0 = PC Webcam, device 1 = external webcam) (PC Florent)
    videoCapture = cv2.VideoCapture(1)
    # global variables and definitions
    prevCircle = None
    lower_blue = np.array([78, 158, 124])
    upper_blue = np.array([138, 255, 255])
    prevX = 0
    prevY = 0
    trigger_low = 3
    trigger_high = 30
    factor = 0.5

    def dist(x1, y1, x2, y2):
        return (x1-x2) ** 2 * (y1-y2) ** 2

    scale = 45

    while True:

        # captures the frame
        ret, frame = videoCapture.read()

        # if there is nothing  we break
        if not ret:
            break

        # get the webcam size
        height, width, channels = frame.shape

        # prepare the crop
        centerX, centerY = int(height / 2), int(width / 2)
        radiusX, radiusY = int(scale * height / 100), int(scale * width / 100)

        minX, maxX = centerX - radiusX, centerX + radiusX
        minY, maxY = centerY - radiusY, centerY + radiusY

        cropped = frame[minX:maxX, minY:maxY]
        resized_cropped = cv2.resize(cropped, (width, height))

        # bluring ans color masking around blue
        # ----------------------------------------

        # converting the RGB frame To HSV
        maskFrame0 = cv2.cvtColor(resized_cropped, cv2.COLOR_BGR2HSV)
        # masking everything exept what is in the blue spectrum
        maskFrame1 = cv2.inRange(maskFrame0, lower_blue, upper_blue)
        # bluring the result to avoid noise
        blurMaskFrame = cv2.GaussianBlur(maskFrame1, (5, 5), 0)

        # method that allows to detetct circles
        # mergeProba=1,mindistance = 1000,High_is_Low_proba =50, accuracy =10, minRadius=1, maxRadius=300
        circles = cv2.HoughCircles(blurMaskFrame, cv2.HOUGH_GRADIENT, 1, 500, param1=40, param2=6, minRadius=1,
                                   maxRadius=300)

        # detecting the circle that moves the least between 2 frames so
        if circles is not None:
            circles = np.uint16(np.around(circles))
            theChosenOne = None
            for i in circles[0, :]:
                if theChosenOne is None: theChosenOne = i
                if prevCircle is not None:
                    if dist(theChosenOne[0], theChosenOne[1], prevCircle[0], prevCircle[1]) <= dist(i[1], i[2],
                                                                                                    prevCircle[0],
                                                                                                    prevCircle[1]):
                        theChosenOne = i
            # drawing the radius and the center of the circle on the frame
            cv2.circle(resized_cropped, (theChosenOne[0], theChosenOne[1]), 1, (255, 0, 0), 2)
            cv2.circle(resized_cropped, (theChosenOne[0], theChosenOne[1]), theChosenOne[2], (0, 255, 0), 2)
            prevCircle = theChosenOne;

            # if there is no significant change in the position we do not send the new data
            if theChosenOne[0] > prevX + trigger_low or theChosenOne[0] < prevX - trigger_low or theChosenOne[
                1] > prevY + trigger_low or theChosenOne[1] < prevY - trigger_low:
                # if the change in position is too big it's a glitch and we don not send either
                prevX = theChosenOne[0]
                prevY = theChosenOne[1]
                # putting the data in a vector 3
                startPos = [-factor * (theChosenOne[0] * 0.0007 - 0.2257), - factor * (theChosenOne[1] * -0.0007 + 0.1652), - factor * 0.0066]
                # converting the vector 3 in string
                posString = ','.join(map(str, startPos))
                # sending the string
                sock.sendall(posString.encode(
                    "UTF-8"))  # Converting string to Byte, and sending it to C#    POUR INFO, SENDALL = UDP SEND = TCP.
                # print("X : ", theChosenOne[0], "; Y : ", theChosenOne[1])

        # displaying the video
        cv2.imshow('circles', resized_cropped)

        # print("                       " + receivedData)

        # if we press Q the programm must stop
        if cv2.waitKey(1) & 0xff == ord('q'): break


    videoCapture.release()
    cv2.destroyAllWindows


if __name__ == '__main__':
    ball_tracking()
