using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Vuforia;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public class ExportT : MonoBehaviour  
{
    string filename = "";

    // to do the positionning of the ball
    public GameObject ball;
    public GameObject spawnPoint;
    public Vector3 prevPos = new Vector3(0,0,0);
    // to have an idea of the time
    public long now = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

    Thread mThread;
    public string connectionIP = "127.0.0.1";
    public int connectionPort = 25001;
    IPAddress localAdd;
    TcpListener listener;
    TcpClient client;
    Vector3 receivedPos = Vector3.zero;

    bool running;
    int frame;


    public class plane
    {
        public float pitch;
        public float roll;
        public float yaw;
    }

    plane p = new plane();

    private void Update()
    {
   	  // updates only if a new position has arrived !
        if (receivedPos + spawnPoint.transform.position != prevPos)
        {
            prevPos =  spawnPoint.transform.position + receivedPos;
            if (prevPos.y < 0.006)
            {
                prevPos.y  = prevPos.y + 0.001f;	// this will eliminate the drifting problem
            }     
	  ball.transform.position = prevPos; //assigning receivedPos in SendAndReceiveData()      
        }



        frame ++;

        p.pitch =  transform.localRotation.eulerAngles.x;
        p.roll = transform.localRotation.eulerAngles.y;
        p.yaw = transform.localRotation.eulerAngles.z;

	  //We write the data on a .csv file to see the noise on the rotations to see uf it's usable or not
        filename = Application.dataPath + "/test.csv";
        TextWriter tw = new StreamWriter(filename,true);
        tw.WriteLine(p.pitch + " , " +p.roll + " , " +p.yaw + " , " + frame);
        tw.Close();
   }

   private void Start()
    {
        ThreadStart ts = new ThreadStart(GetInfo);
        mThread = new Thread(ts);
        mThread.Start();
    }



   void GetInfo()
    {
        localAdd = IPAddress.Parse(connectionIP);
        listener = new TcpListener(IPAddress.Any, connectionPort);
        listener.Start();

        client = listener.AcceptTcpClient();

        running = true;
        while (running)
        {
		SendAndReceiveData();
        }
        listener.Stop();
    }



   void SendAndReceiveData()
    {
        NetworkStream nwStream = client.GetStream();
        byte[] buffer = new byte[client.ReceiveBufferSize];

        //---receiving Data from the Host----
        int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize); //Getting data in Bytes from Python
        string dataReceived = Encoding.UTF8.GetString(buffer, 0, bytesRead); //Converting byte data to string

        if (dataReceived != null)
        {
            //---Using received data---
		//"True" means that we want the rotations of the AR. 
		if (dataReceived == "True"){
			print("Received signal " + dataReceived +  " from the Python script, replying with rotations");
           		//---Sending Data to Host----
            	byte[] myWriteBuffer = Encoding.ASCII.GetBytes(p.pitch + ";" + p.roll + ";" + p.yaw); //Converting string to byte data
            	nwStream.Write(myWriteBuffer, 0, myWriteBuffer.Length); //Sending the data in Bytes to Python
		}
		//else is when we received the coordinate to replace the ball on the AR
		else{
            receivedPos = StringToVector3(dataReceived); //<-- assigning receivedPos value from Python
		}   
        }
    }

   public static Vector3 StringToVector3(string sVector)
    {
        // Remove the parentheses
        if (sVector.StartsWith("(") && sVector.EndsWith(")"))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

       // split the items
        string[] sArray = sVector.Split(',');

       // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

       return result;
    }
}