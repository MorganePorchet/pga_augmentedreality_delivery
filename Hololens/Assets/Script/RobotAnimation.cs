using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RobotAnimation : MonoBehaviour

{
    Animator theAnimator;
    // Start is called before the first frame update
    void Start()
    {
        theAnimator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void RobotMove()
    {
          theAnimator.Play("Default",-1,0f);
        theAnimator.speed = 1f;
    }
}
